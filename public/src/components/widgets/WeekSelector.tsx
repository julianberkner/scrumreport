import clsx from "clsx";
import React, {useState, FC} from "react";
import {DatePicker} from "@material-ui/pickers";
import {createStyles} from "@material-ui/styles";
import {IconButton, Theme, withStyles} from "@material-ui/core";
import {getWeek, format, isValid, endOfWeek, isSameDay, isWithinInterval, startOfWeek, setWeek} from "date-fns";
import {DatePickerProps} from "@material-ui/pickers/DatePicker/DatePicker";
import {makeStyles} from "@material-ui/core/styles";
import DialogContext from "../../context/DialogContext";

const cloneDate = (date: Date) => new Date(date.getTime());

type WeekSelectorProps = Omit<DatePickerProps, 'value' | 'onChange' | 'renderDay' | 'labelFunc'>

export default function WS(props: WeekSelectorProps) {
    const classes = useStyles();
    const contextData = React.useContext(DialogContext);
    const [selectedDate, setSelectedDate] = useState(setWeek(new Date(), contextData.week));
    const handleWeekChange = (date: Date) => {
        setSelectedDate(startOfWeek(cloneDate(date)));
        contextData.setWeek(getWeek(startOfWeek(cloneDate(date))));
    };

    const formatWeekSelectLabel = (date: Date, invalidLabel: string) => {
        let dateClone = cloneDate(date);

        return dateClone && isValid(dateClone)
            ? `Week ${getWeek(dateClone)}`
            : invalidLabel;
    };
    const renderWrappedWeekDay = (date: Date, selectedDate: Date, dayInCurrentMonth: boolean) => {
        let dateClone = cloneDate(date);
        let selectedDateClone = cloneDate(selectedDate);

        const start = startOfWeek(selectedDateClone);
        const end = endOfWeek(selectedDateClone);

        const dayIsBetween = isWithinInterval(dateClone, {start, end});
        const isFirstDay = isSameDay(dateClone, start);
        const isLastDay = isSameDay(dateClone, end);

        const wrapperClassName = clsx({
            [classes.highlight]: dayIsBetween,
            [classes.firstHighlight]: isFirstDay,
            [classes.endHighlight]: isLastDay,
        });

        const dayClassName = clsx(classes.day, {
            [classes.nonCurrentMonthDay]: !dayInCurrentMonth,
            [classes.highlightNonCurrentMonthDay]: !dayInCurrentMonth && dayIsBetween,
        });

        return (
            <div className={wrapperClassName}>
                <IconButton className={dayClassName}>
                    <span> {format(dateClone, "d")} </span>
                </IconButton>
            </div>
        );
    };
    return (
        <DatePicker
            {...props}
            value={selectedDate}
            // @ts-ignore
            onChange={handleWeekChange}
            // @ts-ignore
            renderDay={renderWrappedWeekDay}
            // @ts-ignore
            labelFunc={formatWeekSelectLabel}
        />
    );
}
const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        dayWrapper: {
            position: "relative",
        },
        day: {
            width: 36,
            height: 36,
            fontSize: theme.typography.caption.fontSize,
            margin: "0 2px",
            color: "inherit",
        },
        customDayHighlight: {
            position: "absolute",
            top: 0,
            bottom: 0,
            left: "2px",
            right: "2px",
            border: `1px solid ${theme.palette.secondary.main}`,
            borderRadius: "50%",
        },
        nonCurrentMonthDay: {
            color: theme.palette.text.disabled,
        },
        highlightNonCurrentMonthDay: {
            color: "#676767",
        },
        highlight: {
            background: theme.palette.primary.main,
            color: theme.palette.common.white,
        },
        firstHighlight: {
            extend: "highlight",
            borderTopLeftRadius: "50%",
            borderBottomLeftRadius: "50%",
        },
        endHighlight: {
            extend: "highlight",
            borderTopRightRadius: "50%",
            borderBottomRightRadius: "50%",
        },
    }));
