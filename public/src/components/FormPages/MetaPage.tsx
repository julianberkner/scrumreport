import React, {SyntheticEvent} from 'react';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';
import {MuiPickersUtilsProvider} from "@material-ui/pickers";
import DateFnsUtils from '@date-io/date-fns';
import WeekSelector from "../widgets/WeekSelector";
import DialogContext from "../../context/DialogContext";


export default function MetaPage() {
    const contextData = React.useContext(DialogContext);
    const [name, setName] = React.useState(contextData.name);
    const changeName = (e: React.ChangeEvent<HTMLInputElement>)  => {
        const newName:string = e.target.value;
        setName(newName);
        contextData.setName(newName);
    };
    return (
        <MuiPickersUtilsProvider utils={DateFnsUtils}>
            <React.Fragment>
                <Grid container spacing={3}>
                    <Grid item xs={12} md={6}>
                        <WeekSelector label={"Week number"} fullWidth/>
                    </Grid>
                    <Grid item xs={12} md={6}>
                        <TextField id="name" label="Name" fullWidth onChange={changeName} value={name}/>
                    </Grid>
                </Grid>
            </React.Fragment>
        </MuiPickersUtilsProvider>

    );
}
