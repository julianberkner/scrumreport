import React from 'react';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import DialogContext from "../../context/DialogContext";
import {Typography} from "@material-ui/core";
import axios from "axios";

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
        },
    }),
);

export default function SuccessPage() {
    const classes = useStyles();
    const contextData = React.useContext(DialogContext);

    axios.post(
        'http://localhost:3004/reports',
        {
            "author": contextData.name,
            "curWeek": contextData.curWeek,
            "folWeek": contextData.folWeek,
            "date": contextData.week
        }
    );

    return (
        <Typography variant="h5" gutterBottom>
            Logged.
        </Typography>
    );
}
