import React from 'react';
import {getWeek} from "date-fns";
const DialogContext = React.createContext({
    week: getWeek(new Date()),
    setName: (color:string) => {},
    setWeek: (num:number) => {},
    name: '',
    curWeek: '',
    folWeek: '',
    setCurWeek: (content:string) => {},
    setFolWeek: (content:string) => {},
});
type DialogContextState = {
    week: number,
    setWeek: (num:number) => void,
    name: string,
    setName: (name:string) => void,
    curWeek: string,
    folWeek: string,
    setCurWeek: (content:string) => void,
    setFolWeek: (content:string) => void,
}
export class DialogContexted extends React.Component<any, DialogContextState> {
    constructor(props:any) {
        super(props);

        this.state = {
            name: '',
            week: getWeek(new Date()),
            curWeek: '',
            folWeek: '',
            setName: this.setName.bind(this),
            setCurWeek: this.setCurWeek.bind(this),
            setFolWeek: this.setFolWeek.bind(this),
            setWeek: this.setWeek.bind(this)
        };
    }
    setCurWeek(content:string) {
        this.setState({
            ...this.state,
            curWeek: content,
        });
    }
    setFolWeek(content:string) {
        this.setState({
            ...this.state,
            folWeek: content,
        });
    }
    setName(name:string) {
        this.setState({
            ...this.state,
            name,
        });
    }
    setWeek(num:number) {
        this.setState({
            ...this.state,
            week: num,
        });
    }

    render() {
        return (
            <DialogContext.Provider value={this.state}>
                {this.props.children}
            </DialogContext.Provider>
        );
    }
}

export default DialogContext;

