import React from 'react';
import {makeStyles} from '@material-ui/core/styles';
import './App.css';
import MainForm from "./components/MainForm";
import Summary from "./components/Summary";
import {AppBar, BottomNavigation, BottomNavigationAction, Toolbar, Typography} from "@material-ui/core";

const useStyles = makeStyles({
    root: {
        width: '100%',
    },
    appBar: {
        position: 'relative',
    },
});

function App() {

    const classes = useStyles();
    const [value, setValue] = React.useState(0);
    return (
        <div className="App">
            <AppBar position="absolute" color="default" className={classes.appBar}>
                <Toolbar>
                    <Typography variant="h6" color="inherit" noWrap>
                        ScrumReport
                    </Typography>
                </Toolbar>
            </AppBar>
            {
                value === 0 &&  <MainForm/>
            }
            {
                value === 1 &&  <Summary/>
            }
            <BottomNavigation
                value={value}
                onChange={(event, newValue) => {
                    setValue(newValue);
                }}
                showLabels
                className={classes.root}
            >
                <BottomNavigationAction label="Log a report"/>
                <BottomNavigationAction label="see the logs"/>
            </BottomNavigation>
        </div>
    );
}

export default App;
