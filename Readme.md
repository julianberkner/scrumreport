# ScrumReport

## Description
ScrumReport is a minimal effort tool to allow team members to log their works report, this includes
* Name 
* Week of year
* Current weeks "done" list
* next week proposed todo list

## Basic Setup
* Check out the git
* `$ cd <project dir>;`
* `$ yarn`
* `$ cd public; yarn; yarn build`
* `$ cd <project dir>;`
* `$ yarn run dev`

## About this project
### Components
This project makes use of 
* [json-server](https://github.com/typicode/json-server)
* [react](https://reactjs.org/)
* [material ui](https://material-ui.com/)
* [material table](https://material-table.com/)

### Usage
#### Basic
After installing the required node modules, you will be presented with json-server ready to act as REST API and to deliver
the static content (e.g. the included react site). To enable json-server to deliver the site, the react project found in 
./public must have been build (Setup pt. 3).

Point your browser to http://localhost:3004 and be good.

#### Advanced
You do not have to build the react project to get going, you can also start the react project as known via `$ yarn start` (in ./public).
Website will then be available at http://localhost:3000 and the API at http://localhost:3004.

### Known restrictions
* The API is totally unprotected. Firewall it if you have to. 
* The UI is not protected. Use traefik (or nginx) to protect it.
* There is no permission setup for the UI. Everyone can report, view reports and download reports.
* Database is a flat file (json) in ./data/db.json. It is plain readable by anyone with file system permissions.
* There is NO backup of anything. Backup is easily done via cronjob / sophisticated backup tools (its a plain file after all).
* This is a made because of a code challenge.

### Docker and stuff
This project can be easily set up for container usage. 
Just push the json-server part in one image and the frontend in another (using eg. nginx).
You could then easily use traefik to include some basic auth and block the api from the outside world.
