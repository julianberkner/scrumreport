import React, {Component} from 'react';
import MaterialTable from 'material-table';
import axios from "axios";
import {getWeek} from "date-fns";

const loadData = async (query:any) => {
    let url = `http://localhost:3004/reports?_start=${query.page * query.pageSize}&_limit=${query.pageSize}`;
    const dateFilter = query.filters.filter((f:any) => f.column.field === 'date');
    if (dateFilter.length) {
        url += `&date=${getWeek(dateFilter[0].value)}`;
    }
    if (query.orderBy && query.orderDirection) {
        url += `&_sort=${query.orderBy.field}&_order=${query.orderDirection}`;
    }

    const response = await axios.get(url);
    const totalCount = response.headers['x-total-count'];
    return [response.data, query.page, parseInt(totalCount)];
};

export default class Summary extends Component {
    render() {
        return (
            <div style={{maxWidth: '100%'}}>
                <MaterialTable
                    columns={[
                        {title: 'name', field: 'author', filtering: false},
                        {title: 'week #', field: 'date', type: 'date'},
                        {title: 'current week', field: 'curWeek', filtering: false},
                        {title: 'following week', field: 'folWeek', filtering: false}
                    ]}
                    options={{
                        filtering: true,
                        exportButton: true
                    }}
                    data={(query) =>
                        new Promise(async (resolve, reject) => {
                            const [data, page, totalCount] = await loadData(query);
                            resolve({
                                data,
                                page,
                                totalCount,
                            });

                        })
                    }
                    title="Scrum Reporting"
                />
            </div>
        );
    }
}
