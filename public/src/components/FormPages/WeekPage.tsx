import React from 'react';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import DialogContext from "../../context/DialogContext";

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            '& .MuiTextField-root': {
                margin: theme.spacing(1),
                //width: 200,
            },
        },
    }),
);
type SomeWeekProps = {
    placeholder: string,
    label: string,
    contextfield: 'curWeek'|'folWeek',
}
function WeekPage(props:SomeWeekProps) {
    const classes = useStyles();
    const contextData = React.useContext(DialogContext);
    const [value, setValue] = React.useState(contextData[props.contextfield]);

    const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        setValue(event.target.value);
        const contextField = `${props.contextfield[0].toUpperCase()}${props.contextfield.slice(1)}`;
        //@ts-ignore
        const setter:('setCurWeek'|'setFolWeek') = `set${contextField}`;
        contextData[setter](event.target.value);
    };

    return (
        <form className={classes.root} noValidate autoComplete="off">
            <div>
                <TextField
                    {...props}
                    id="outlined-multiline-static"
                    multiline
                    rows="10"
                    fullWidth
                    variant="outlined"
                    value={value}
                    onChange={handleChange}
                />
            </div>
        </form>
    );
}
export function CurrentWeek() {
    return <WeekPage placeholder='Please enter current weeks report' label='Current week' contextfield={'curWeek'}/> ;
}
export function NextWeek() {
    return <WeekPage placeholder='Please enter next weeks todos' label='Next week' contextfield={'folWeek'}/> ;
}
